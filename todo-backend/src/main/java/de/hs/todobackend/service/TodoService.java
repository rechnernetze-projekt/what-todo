package de.hs.todobackend.service;

import de.hs.todobackend.entity.Todo;

import java.util.List;

public interface TodoService {
    List<Todo> getTodos();
    Todo saveTodo(Todo todo);
    void deleteTodo(Todo todo);
    void deleteTodo(Long todoId);
}
