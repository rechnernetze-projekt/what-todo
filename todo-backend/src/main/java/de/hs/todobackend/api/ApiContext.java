package de.hs.todobackend.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiContext {
    private String podName;
    private String podIp;
    private String appVersion;
}
