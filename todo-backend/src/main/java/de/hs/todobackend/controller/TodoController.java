package de.hs.todobackend.controller;

import de.hs.todobackend.api.ApiContext;
import de.hs.todobackend.api.ApiResponse;
import de.hs.todobackend.entity.Todo;
import de.hs.todobackend.service.TodoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class TodoController {
    @Value("${pod-name:localhost}")
    private String podName;

    @Value("${pod-ip:127.0.0.1}")
    private String podIp;

    @Value("${app-version}")
    private String appVersion;

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("todo")
    public ApiResponse<List<Todo>> getTodos() {
        return new ApiResponse<>(todoService.getTodos(), getApiContext());
    }

    @PostMapping("todo")
    public ApiResponse<Todo> saveTodo(@RequestBody Todo todo) {
        return new ApiResponse<>(todoService.saveTodo(todo), getApiContext());
    }

    @DeleteMapping("todo/{todoId}")
    public ApiResponse<Long> deleteTodo(@PathVariable Long todoId) {
        todoService.deleteTodo(todoId);
        return new ApiResponse<>(todoId, getApiContext());
    }

    private ApiContext getApiContext() {
        return new ApiContext(podName, podIp, appVersion);
    }
}
