import axios from 'axios';

export default class BackendService {
  async getTodoList() {
    const response = await axios.get(`${process.env.VUE_APP_BACKEND_URL}/todo`);
    return response;
  }
}
