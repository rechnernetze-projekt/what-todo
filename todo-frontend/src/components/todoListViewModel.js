import Vue from 'vue';
import Component from 'vue-class-component';

@Component()
export default class TodoListViewModel extends Vue {
  todos = []

  context = {}

  checkedRows = []

  secondsToRefresh = 3;

  async mounted() {
    this.prepareModel();
    setInterval(this.refreshTimerTick, 1000);
  }

  async prepareModel() {
    const response = await this.$backendService.getTodoList();
    this.todos = response.data.data;
    this.context = response.data.context;
    console.log(JSON.stringify(response.data));
  }

  refreshTimerTick() {
    if (this.secondsToRefresh === 0) {
      this.prepareModel();
      this.secondsToRefresh = 3;
    } else {
      this.secondsToRefresh -= 1;
    }
  }
}
