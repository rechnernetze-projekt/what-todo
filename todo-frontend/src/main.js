import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import App from './App.vue';
import BackendService from './services/backendService';

Vue.use(Buefy);

Vue.config.productionTip = false;
Vue.prototype.$backendService = new BackendService();

new Vue({
  render: (h) => h(App),
}).$mount('#app');
