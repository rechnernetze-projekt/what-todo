CREATE TABLE todo (
    id serial PRIMARY KEY,
    title varchar(255) NOT NULL,
    description text,
    created_on timestamp NOT NULL DEFAULT NOW(),
    due_on timestamp,
    done boolean DEFAULT false
);

INSERT INTO todo(title, description, due_on) VALUES ('Do The Dishes', 'Nothing much to say here.', NOW() + INTERVAL '2 day');
INSERT INTO todo(title, due_on) VALUES ('Feed The Dog', NOW() + INTERVAL '2 day');
INSERT INTO todo(title, description, due_on, done) VALUES ('Groceries', 'Apples, Flour and Sugar.', NOW() + INTERVAL '10 day' + INTERVAL '2 hour', true);
INSERT INTO todo(title, description, due_on) VALUES ('Workout', 'JUST DO IT.', NOW() - INTERVAL '2 day 3 hour');
INSERT INTO todo(title, description, due_on) VALUES ('Study', 'MPT, GIS ...', NOW() - INTERVAL '1 day 6 hour');
INSERT INTO todo(title, description, due_on, done) VALUES ('Call Mum', '', NOW() + INTERVAL '2 day', true);
INSERT INTO todo(title, description, due_on, done) VALUES ('Networking Project', 'Finally finish the presentation and make the view look good.', NOW() + INTERVAL '5 day', true);